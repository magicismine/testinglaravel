<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//return View::make('halo_juga');
Route::get('/', function(){	return View::make('root');});
//Route::get('/', 'UserController@putLogout');
Route::get('/logout', 'UserController@logout');

Route::post('/users/auth-token', 'UserController@postAuthToken');
Route::put('/users/logout', 'UserController@putLogout');
Route::resource('users', 'UserController');

Route::put('/todolist/{id}/update-user-todo', 'TodolistController@putUpdateUserTodo');
Route::resource('todolist', 'TodolistController');
?>
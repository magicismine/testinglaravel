<?php

class UserTodo extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_todolists';
	public $timestamps = false;
}

?>
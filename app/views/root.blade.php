<!-doctype html>

<html ng-app="App">
	<head>
		<title> - SEO Manager - </title>
		{{ HTML::style('css/bootstrap.css'); }}
		{{ HTML::script('js/angular.js'); }}
		{{ HTML::script('js/angular-route.js'); }}
		{{ HTML::script('js/angular-resource.js'); }}
		{{ HTML::script('js/helper_services.js'); }}
		{{ HTML::script('js/app.js'); }}
		{{ HTML::script('js/jquery.js'); }}
		{{ HTML::script('js/main.js'); }}
		{{ HTML::script('js/bootstrap.min.js'); }}
	</head>
	<?php //var_dump(Session::all()); ?>
	<body ng-controller="rootController" ng-init="initUser()">
		<div ng-view></div>
	</body>
</html>
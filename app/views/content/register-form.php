		

		<div class="jumbotron" ng-controller="registerController">
			<h3> Register Page </h3>
			<form class="form-horizontal" role="form" ng-submit="userRegister()">
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> Email :  </p></div>
				    <div class="col-xs-3">
			      		<input type="email" class="form-control" placeholder="Email Address" ng-model="mailRegistry" required>
			    	</div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> name :  </p></div>
				    <div class="col-xs-3">
			      		<input type="text" class="form-control" placeholder="Name" ng-model="nameRegistry" required>
			    	</div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> Username : </p></div>
				    <div class="col-xs-3">
			      		<input type="text" class="form-control" placeholder="Username" ng-model="unameRegistry" required>
			    	</div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> Password : </p></div>
				    <div class="col-xs-3">
			      		<input type="password" class="form-control" placeholder="Password" ng-model="passRegistry" required>
			    	</div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> Confirm Password : </p></div>
				    <div class="col-xs-3">
			      		<input type="password" class="form-control" placeholder="Confirm Password" ng-model="cpassRegistry"  required>  
			    	</div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> Web Address : </p></div>
				    <div class="col-xs-3">
			      		<input type="text" class="form-control" placeholder="Web Adress" ng-model="webAddressRegistry"  required>
			    	</div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-sm-offset-2 col-xs-1">
				      	<button type="submit" class="btn btn-info">Sign Up</button>
				    </div>
				    <div class="col-xs-1">
				      	<a class="btn btn-warning" href="#/">Back</a>
				    </div>
			  	</div>
			</form>
			<div class="alert alert-warning" ng-show="isError"> {{message}} </div>
		</div>
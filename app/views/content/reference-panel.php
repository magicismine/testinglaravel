
<div ng-controller="homeController" ng-init="initUser()">
	
	<!-- HEADER -->
	<nav class="navbar navbar-default">
		<div class="navbar-header">	<a class="navbar-brand" href="/home">SEO Manager</a></div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li> <a href="#" ng-click="logout()"> <span class="glyphicon glyphicon-off"></span> Sign Out  </a></li>
			</ul>
		</div>
	</nav>
	<!-- NAVBAR -->
	<div class="col-xs-3">
		<div class="panel panel-info">
			<div class="panel-heading"> <h4> Welcome, {{user.username}}. </h4></div>
			<div class="panel-body">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/testinglaravel/public/#/home/todolist"> Todo List</a></li>
					<li class="active"><a href="/testinglaravel/public/#/home/db"> Reference </a></li>
					<li><a href="/testinglaravel/public/#/home/content-marketing"> Content Marketing </a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- CONTENT -->
	<div class="col-xs-9" ng-controller="referenceController">		
		<div class="panel panel-primary">
			<div class="panel-heading"> <h1> Reference Manager </h1> </div>
			<div class="panel-body">
				This is the list of url which you doesn't have yet.
			</div>
		</div>
	</div>
</div>
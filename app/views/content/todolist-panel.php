<div ng-controller="homeController">

	<!-- HEADER -->
	<nav class="navbar navbar-default">
		<div class="navbar-header">	<a class="navbar-brand" href="/home">SEO Manager</a></div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li> <a href="#" ng-click="logout()"> <span class="glyphicon glyphicon-off"></span> Sign Out  </a></li>
			</ul>
		</div>
	</nav>

	<!-- NAVBAR -->
	<div class="col-xs-3">
		<div class="panel panel-info">
			<div class="panel-heading"> <h4> Welcome, {{user.username}}. </h4></div>
			<div class="panel-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="/testinglaravel/public/#/home/todolist"> Todo List</a></li>
					<li><a href="/testinglaravel/public/#/home/db"> Reference </a></li>
					<li><a href="/testinglaravel/public/#/home/content-marketing"> Content Marketing </a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- CONTENT -->
	<div class="col-xs-9" ng-controller="todolistController">		
		<div class="panel panel-primary">
			<div class="panel-heading"> <h1> Todo list </h1> </div>
			<div class="panel-body">
				
			<table class="table table-bordered table-striped">
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Type</th>
					<th>Url</th>
					<th>Finished</th>
				</tr>
				<tr ng-repeat="t in todolists track by $index">
					<td> {{t.id}} </td>
					<td> {{t.name}} </td>
					<td> {{t.type}} </td>
					<td> {{t.url}} </td>
					<td> <input type="checkbox" ng-model="t.isFinished" ng-true-value="1" ng-false-value="0" ng-change="editUserTodo(t)"></input></td>
				</tr>
			</table>
			</div>
		</div>
	</div>
</div>
<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function postAuthToken(){
		// LOAD INPUT
		$temp = Input::all();
		// FIND USERNAME AND PASSWORD IN TABLE USER MATCH
		$O = User::where('username', $temp['uname'])->where('password', $temp['upass'])->get();
		// FILTERING DATA IF IT IS NULL OR DATA NOT FOUND
		$filter = array_filter($O->toArray());
		if(!empty($filter)){	
			// DATA FOUND, RANDOMIZE TOKEN
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$res = array('token' => $token);
			// SET SESSION TOKEN AND SESSION USER ID
			$user = User::find($O->toArray()[0]['id']);
			Session::put('user.token', $token);
			Session::put('user.id', $user->id);
		}
		else{
			$res = array('token' => "0");
		}
		return json_encode($res);
	}

	public function putLogout(){
		// LOGOUT USER
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			// SUCCESS LOGOUT	
			//Session::flush();
			Session::forget('user.token');
			Auth::logout(); 
			return 1;
		}
		return 0;
	}

	public function index(){
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			// CHECK IF USER IS LOG IN
			$O = User::find(Session::get('user.id', '0'));
			$filter = array_filter($O->toArray());
			if(!empty($filter)){
				$res = $O->toArray();
				return json_encode($res);
			}
		}
		return 0;
	}

	public function create(){
		echo "create";
	}

	public function store(){
		// INPUT TO TABLE USER
		$O = new User;
		$O->username = Input::get('username');
		$O->name = Input::get('name');
		$O->email 	 = Input::get('email');
		$O->password = Input::get('password');
		$O->webAddress = Input::get('webAddress');
		$O->save();
		// RETURN NEW ID
		return $O->id;
	}

	public function show($id){
		// VIEW SPECIFIC USER
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			// CHECK IF USER IS LOG IN
			$O = User::find($id);
			$filter = array_filter($O->toArray());
			if(!empty($filter)){
				$res = $O->toArray();
				return json_encode($res);
			}
		}
		return 0;
	}

	public function edit($id){
		echo "edit : ". $id;
	}

	public function update($id){
	    echo "update";
	}
}

<?php

class TodolistController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function putUpdateUserTodo($id){
		// UPDATE TO TABLE = 'users_todolists'
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			// CHECK IF USER IS LOG IN
			$mode = Input::get('isFinished');
			if($mode == 0){
				$res = UserTodo::where('user_id', $id)->where('todo_list_id', Input::get('id'))->delete();
			}
			else{
				$userTodo = new UserTodo;
				$userTodo->user_id = $id;
				$userTodo->todo_list_id = Input::get('id');
				$userTodo->last_update = date('Y-m-d');
				$res = $userTodo->save();
			}
			return ($res) ? '1' : '0';
		}
		return 0;
	}

	public function index(){
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			$O = Todolist::all();
			$filter = array_filter($O->toArray());
			if(!empty($filter)){
				return json_encode($O->toArray());
			}
		}
		return 0;
	}

	public function create(){
		echo "create";
	}

	public function store(){
		echo "store";
	}

	public function show($id){
		// VIEW SPECIFIC TODOLIST
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			// CHECK IF USER IS LOG IN
			$O = Todolist::all();
			$filter = array_filter($O->toArray());
			$res = null;
			if(!empty($filter)){
				$res = $O->toArray();
				$O = UserTodo::where('user_id', $id)->get();
				$filter = array_filter($O->toArray());
				if(empty($filter)){
					for($i=0;$i<count($res);$i++){
						$res[$i]['isFinished'] = '0';
					}
				}
				else{
					$temp = $O->toArray();
					for($i=0;$i<count($res);$i++){
						for($j=0;$j<count($temp);$j++){
							if($res[$i]['id'] == $temp[$j]['todo_list_id']){
								$res[$i]['isFinished'] = '1';break;
							}
							else
								$res[$i]['isFinished'] = '0';
						}
					}
				}
				return json_encode($res);
			}
		}
		return 0;
	}

	public function edit($id){
		echo "edit : ". $id;
	}

	public function update($id){
	    echo "update : ". $id;
	}
}

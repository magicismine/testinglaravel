var myApp = angular.module('App', [
  'ng',
  'ngRoute',
  'mainModule',
  'HelperServices'
]);


myApp.config(['$routeProvider', '$locationProvider',function($routeProvider, $locationProvider) {
    $routeProvider
      	.when('/login',
      	{
        	templateUrl: "/testinglaravel/app/views/content/login-form.php"
      	})
      	.when('/register',
      	{
        	templateUrl: "/testinglaravel/app/views/content/register-form.php"
      	})
        .when('/home/db',
        {
          templateUrl: "/testinglaravel/app/views/content/reference-panel.php"
        })
        .when('/home/content-marketing',
        {
          templateUrl: "/testinglaravel/app/views/content/content-marketing-panel.php"
        })
        .when('/home/todolist',
        {
          templateUrl: "/testinglaravel/app/views/content/todolist-panel.php"
        })
      	.otherwise({
        	redirectTo: "/login"
      	})

    // $locationProvider.html5Mode(true);
}]);
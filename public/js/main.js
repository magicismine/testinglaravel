
var index = angular.module('mainModule',[]);

index.controller('rootController',['$scope','$http','Helper','$q', '$location',
	function($scope, $http, Helper, $q, $location){
		$scope.name = "this is root controller";
	}
]);

index.controller('loginController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
		$scope.name = "this is login controller";
		$scope.isError = false;
		$scope.userLogin = function(){
			$scope.param = { 'uname' : $scope.username, 'upass' : $scope.userpassword };
			Helper
			.login("POST", "http://localhost/testinglaravel/public/users/auth-token", $scope.param)
			.then(function(data){
				if(data.token != "0"){
					// SUCCESS LOGIN
					console.log("SUCCESS > ", data);
					$window.sessionStorage.token = data.token;
					$location.path('/home/todolist');
				}
				else{
					console.log(data);
					$scope.isError = true;
				}
			});
		};
	}
]);

index.controller('registerController', ['$scope','$http','Helper','$q', '$location', '$timeout', 
	function($scope, $http, Helper, $q, $location, $timeout){
		$scope.message = "";
		$scope.isError = false;
		$scope.userRegister = function(){
			if($scope.passRegistry == $scope.cpassRegistry && typeof $scope.mailRegistry != "undefined"){
				$scope.param = { 
					'username' : $scope.unameRegistry, 
					'name' : $scope.nameRegistry, 
					'email' : $scope.mailRegistry, 
					'password' : $scope.passRegistry, 
					'webAddress' : $scope.webAddressRegistry
				};
				Helper
				.register("POST", "http://localhost/testinglaravel/public/users", $scope.param)
				.then(function(data){
					// SUCCESS ADD NEW USER
					$scope.message = "Congratulation data has been added, you'll be redirect to login page.";
					$scope.isError = true;
					$timeout(function() {
						$location.path("/login");
					}, 2000);
				}, function(data){
					// ERROR ADD NEW USER
					$scope.isError = true;
					console.log(data);
				});
			}
			else{
				$scope.message = "Error, register doesn't finished.";
				$scope.isError = true;
			}
		};
	}
]);

index.controller('homeController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
		$scope.token = null;
		$scope.user = null;
		$scope.todolists = {};

		if(typeof $window.sessionStorage.token == "undefined")
			$location.path('/login');

		// INIT ALL INFO TO MAKE SURE THAT IS SYNCRHONOUSLY LOAD
		// INIT LOAD USER INFO
		Helper
		.getUserInfo("GET", "http://localhost/testinglaravel/public/users", "")
		.then(function(data){
			$scope.user = data;

			// INIT USER TODOLIST
			Helper
			.getTodolist("GET", ("http://localhost/testinglaravel/public/todolist/" + $scope.user.id), "")
			.then(function(data){
				if(data != null){
					$scope.todolists = data;
				}
				else
					console.log("Error, Todolist doesn't have any record. s")
			}, function(data){
				console.log(data);
			});
		}, function(data){
			console.log(data);
		});

		$scope.logout = function(){
			Helper
			.logout("PUT", "http://localhost/testinglaravel/public/users/logout", "")
			.then(function(data){
				if(data == 1){
					// SUCCESS LOG OUT
					console.log('SUCCESS > Logout.');
					// DELETE TOKEN STORAGE
					delete $window.sessionStorage.token;
					// REDIRECT TO LOGIN PAGE
					$location.path('/login');
				}
			}, function(data){
				console.log(data);
			});
		};
	}
]);

index.controller('todolistController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
		$scope.editUserTodo = function(t){
			Helper
			.editUserTodo("PUT", ("http://localhost/testinglaravel/public/todolist/" + $scope.user.id + "/update-user-todo"), t)
			.then(function(data){
				if(data == 1){
					// SUCCESS EDIT USER DATA
					console.log("SUCCESS EDIT > ", data);
				}
			}, function(data){
				console.log(data);
			});
		};
	}
]);

index.controller('referenceController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
	}
]);

index.controller('contentMarketController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
	}
]);